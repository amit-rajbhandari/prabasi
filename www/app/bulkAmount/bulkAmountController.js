angular.module('starter').controller('bulkAmountController', ['$scope', '$state', '$rootScope', 'localStorageService', '$window', '$ionicPopup', function($scope, $state, $rootScope, localStorageService, $window, $ionicPopup) {

    // $scope.selectedLanguage = $rootScope.language;
    $scope.selectedLanguage = "English";
    $scope.bulkAmountData = {};
    $scope.disabled = true;


    if (localStorageService.get('BulkAmountData') != null) {
        $scope.bulkAmountData.bulkAmount = localStorageService.get('BulkAmountData').BulkAmount;
        $scope.bulkAmountData.Passport = localStorageService.get('BulkAmountData').Passport;
        $scope.bulkAmountData.Health_Checkup = localStorageService.get('BulkAmountData').HealthCheckup;
        $scope.bulkAmountData.Skill_Development = localStorageService.get('BulkAmountData').SkillDevelopment;
        $scope.bulkAmountData.Manpower_Costs = localStorageService.get('BulkAmountData').ManpowerCost;
        $scope.bulkAmountData.Visa_Request = localStorageService.get('BulkAmountData').VisaRequest;
        $scope.bulkAmountData.Air_Ticket = localStorageService.get('BulkAmountData').Airticket;
        $scope.bulkAmountData.Insurance = localStorageService.get('BulkAmountData').Insurance;
        $scope.bulkAmountData.Welfare_Fund = localStorageService.get('BulkAmountData').WelfareCost;
        $scope.bulkAmountData.Labor_Permit = localStorageService.get('BulkAmountData').LaborPermit;
        $scope.bulkAmountData.Pre_Orientation = localStorageService.get('BulkAmountData').PreOrientationTrainning;


    }









    $scope.bulkSubmit = function(isValid) {

        if (isValid) {
            var BulkAmount = $scope.bulkAmountData.bulkAmount;
            var Passport = $scope.bulkAmountData.Passport;
            var Checkup = $scope.bulkAmountData.Health_Checkup;
            var Skill = $scope.bulkAmountData.Skill_Development;
            var Manpower = $scope.bulkAmountData.Manpower_Costs;
            var Visa = $scope.bulkAmountData.Visa_Request;
            var Airticket = $scope.bulkAmountData.Air_Ticket;
            var Insurance = $scope.bulkAmountData.Insurance;
            var Welfare = $scope.bulkAmountData.Welfare_Fund;
            var Labor = $scope.bulkAmountData.Labor_Permit;
            var Pre_Orientation = $scope.bulkAmountData.Pre_Orientation;



            if ((Passport == undefined || Passport == false) && (Checkup == undefined || Checkup == false) && (Skill == undefined || Skill == false) && (Manpower == undefined || Manpower == false) && (Visa == undefined || Visa == false) && (Airticket == undefined || Airticket == false) && (Insurance == undefined || Insurance == false) && (Welfare == undefined || Welfare == false) && (Labor == undefined || Labor == false) && (Pre_Orientation == undefined || Pre_Orientation == false)) {
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'Please check at least one option below.'
                });

                return false;
            } else {
                var bulkAmountDatas = {
                    BulkAmount: BulkAmount,
                    Passport: Passport,
                    HealthCheckup: Checkup,
                    SkillDevelopment: Skill,
                    ManpowerCost: Manpower,
                    VisaRequest: Visa,
                    Airticket: Airticket,
                    Insurance: Insurance,
                    WelfareCost: Welfare,
                    LaborPermit: Labor,
                    PreOrientationTrainning: Pre_Orientation
                }
                localStorageService.set('BulkAmountData', bulkAmountDatas);

                /* manpower cost*/
                if (localStorageService.get('manpowerData') != null) {
                    if (Manpower != undefined || Manpower != false) {
                        var manpower = localStorageService.get('manpowerData');
                        manpower.manpowerCost = BulkAmount;
                        manpower.TotalCost = BulkAmount + localStorageService.get('manpowerData').additionalCost;
                        localStorageService.set('manpowerData', manpower);

                    }


                }
                /*passport cost*/

                // if (localStorageService.get('PassportCost') != null) {

                //     if (Passport != undefined || Passport != false) {
                //         var passport = localStorageService.get('PassportCost');
                //         // passport.passportCost = 0;
                //         passport.TotalCost = localStorageService.get('PassportCost').extraCost;
                //         localStorageService.set('PassportCost', passport);

                //     }


                // }


                // /* heatlth checkup*/
                // if (localStorageService.get('HealthCheckup') != null) {
                //     if (Checkup != undefined || Checkup != false) {
                //         var healthcheckup = localStorageService.get('HealthCheckup');
                //         healthcheckup.healthCheckupFee = 0;
                //         healthcheckup.TotalCost = localStorageService.get('HealthCheckup').additionalCost;
                //         localStorageService.set('HealthCheckup', healthcheckup);


                //     }



                // }


                // /*skill development*/
                // if (localStorageService.get('skillDevelopment') !== null) {
                //     if (Skill != undefined || Skill != false) {
                //         var skill = localStorageService.get('skillDevelopment');
                //         skill.trainingFee = 0;
                //         skill.TotalCost = localStorageService.get('skillDevelopment').additionalCost;
                //         localStorageService.set('skillDevelopment', skill);

                //     }



                // }
                

                /*welfare fund*/

                // if (localStorageService.get('welfareCost') != null) {

                //     if (Welfare != undefined || Welfare != false) {
                //         var welfare = localStorageService.get('welfareCost');
                //         welfare.fundCost = 0;
                //         welfare.TotalCost = localStorageService.get('welfareCost').additionalCost;
                //         localStorageService.set('welfareCost', welfare);

                //     }


                // }


                // /*Pre Orientation trainning*/
                // if (localStorageService.get('PreOrientationData') != null) {

                //     if (Pre_Orientation != undefined || Pre_Orientation != false) {
                //         var preOrientation = localStorageService.get('PreOrientationData');
                //         preOrientation.trainningCost = 0;
                //         preOrientation.TotalCost = localStorageService.get('PreOrientationData').additionalCost;
                //         localStorageService.set('PreOrientationData', preOrientation);

                //     }


                // }



                // /*Insurance */
                // if (localStorageService.get('InsuranceCost')!=null) {

                //     if (Insurance != undefined || Insurance != false) {
                //         var insurance = localStorageService.get('InsuranceCost');
                //         insurance.premiumAmount = 0;
                //         insurance.TotalCost = localStorageService.get('InsuranceCost').extraCost;
                //         localStorageService.set('InsuranceCost', insurance);

                //     }


                // }


                // /*labour permit */
                // if (localStorageService.get('labourData') != null) {
                //     if (Labor != undefined || Labor != false) {
                //         var labour = localStorageService.get('labourData');
                //         labour = 0;
                //         localStorageService.set('labourData', labour);

                //     }



                // }

                // /*Air ticket */
                // if (localStorageService.get('AirTicket') != null) {
                //     if (Airticket != undefined || Airticket != false) {
                //         var airticket = localStorageService.get('AirTicket');
                //         airticket = 0;
                //         localStorageService.set('AirTicket', airticket);

                //     }

                // }


                // /*visa request*/
                // if (localStorageService.get('VisaRequest') != null) {

                //     if (Visa != undefined || Visa != false) {
                //         var visa = localStorageService.get('VisaRequest');
                //         visa = 0;
                //         localStorageService.set('VisaRequest', visa);

                //     }

                // }






                $state.transitionTo('app.dashboard', null, { 'reload': true });






            }
        } else {
            $scope.submitted = true;
        }

    }


    // if (localStorageService.get('contractData') !== null) {


    //     $scope.contractData.employeeName = localStorageService.get('contractData').employeeName;


    // }


    // if ($scope.contractData.employeeName) {
    //     $scope.disabled = false;

    // }









}])
