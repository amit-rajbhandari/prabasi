angular.module('starter').controller('healthCheckups', ['$scope', '$ionicModal', '$rootScope', '$timeout', '$ionicPopup', 'localStorageService', '$state', healthCheckups]);

function healthCheckups($scope, $ionicModal, $rootScope, $timeout, $ionicPopup, localStorageService, $state) {
    // $scope.selectedLanguage = $rootScope.language;
    $scope.selectedLanguage = "English";
    $scope.lang = $scope.selectedLanguage;
    $scope.healthcenters = [];
    $scope.readonly = false;
    var limit = 50;
    $scope.from = 0;
    $scope.to = $scope.from + limit;
    $ionicModal.fromTemplateUrl('app/healthCheckups/health-checkups-center.html', {
        scope: $scope
    })

    .then(function(modal) {
        $scope.healthCenter = modal;
    });

    $scope.closeModal = function() {
        $scope.healthCenter.hide();
    };

    $scope.healthcenterData = {
        selectedHealthCenter: null,
        healthCheckupFee: null,
        additionalCost: null
    };

    function init() {
        var result = getHealthCenter($scope.from, $scope.to, $scope.lang);
        for (var i = 0; i < result.length; i++) {
            $scope.healthcenters.push(result[i]);
        }
    }

    init();
    $scope.loadmoreHealthCenter = function() {
        $scope.from = $scope.to;
        $scope.to = $scope.from + limit;
        var result = getHealthCenter($scope.from, $scope.to, $scope.lang);
        for (var i = 0; i < result.length; i++) {
            $scope.healthcenters.push(result[i]);
        }
    };

    if (localStorageService.get('HealthCheckup') !== null) {
        $scope.healthcenterData.selectedHealthCenter = localStorageService.get('HealthCheckup').selectedHealthCenter;
        $scope.healthcenterData.healthCheckupFee = localStorageService.get('HealthCheckup').healthCheckupFee;
        $scope.healthcenterData.additionalCost = localStorageService.get('HealthCheckup').additionalCost;
    }


    if (localStorageService.get('BulkAmountData') != null) {

        if (localStorageService.get('BulkAmountData').HealthCheckup != undefined && localStorageService.get('BulkAmountData').HealthCheckup != false) {

            $scope.readonly = true;
            $scope.healthcenterData.healthCheckupFee = 0;
            if (localStorageService.get('HealthCheckup') !== null) {
                $scope.healthcenterData.selectedHealthCenter = localStorageService.get('HealthCheckup').selectedHealthCenter;
                $scope.healthcenterData.additionalCost = localStorageService.get('HealthCheckup').additionalCost;

            }

        }

    } else {
        if (localStorageService.get('HealthCheckup') !== null) {
            $scope.healthcenterData.selectedHealthCenter = localStorageService.get('HealthCheckup').selectedHealthCenter;
            $scope.healthcenterData.healthCheckupFee = localStorageService.get('HealthCheckup').healthCheckupFee;
            $scope.healthcenterData.additionalCost = localStorageService.get('HealthCheckup').additionalCost;


        }


    }




    $scope.HealthCheckupSubmit = function(isValid) {

        if (isValid) {
            var healthCenter = $scope.healthcenterData.selectedHealthCenter;
            if (healthCenter == null || healthCenter == '') {
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'Please select health center.'
                });

            } else {
                var healthCheckupFee = $scope.healthcenterData.healthCheckupFee;
                var additionalCost = $scope.healthcenterData.additionalCost;
                var TotalCost = healthCheckupFee + additionalCost;


                var healthcenterData = {
                    selectedHealthCenter: $scope.healthcenterData.selectedHealthCenter,
                    healthCheckupFee: $scope.healthcenterData.healthCheckupFee,
                    additionalCost: $scope.healthcenterData.additionalCost,
                    TotalCost: TotalCost

                }
                localStorageService.set('HealthCheckup', healthcenterData);

                $state.transitionTo('app.dashboard', null, { 'reload': true });








            }


        } else {
            $scope.submitted = true;
        }
    }





}
