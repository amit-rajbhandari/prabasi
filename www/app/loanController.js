angular.module('starter').controller('loanController', ['$scope', '$rootScope', 'localStorageService', '$state', '$ionicPopup', loanController]);

function loanController($scope, $rootScope, localStorageService, $state, $ionicPopup) {
    $scope.loanData = {};



    $scope.onChange = function() {

        var loanAmoutRequired = $scope.loanData.totalLoanAmount;
        var handCash = $scope.loanData.handCash;
        if (handCash == undefined || handCash == null) {
            handCash = 0;
        }
        $scope.loanData.loanAmount = loanAmoutRequired - handCash;


    }


    $scope.onChangeLoanData = function() {

        var bankIntrestOne = 0;
        var bankIntrestTwo = 0;
        var bankIntrestThree = 0;
        var bankAmountOne = 0;
        var bankAmountTwo = 0;
        var bankAmountThree = 0;
        var relativeIntrestOne = 0;
        var relativeIntrestTwo = 0;
        var relativeIntrestThree = 0;
        var relativeAmountOne = 0;
        var relativeAmountTwo = 0;
        var relativeAmountThree = 0;
        var NoOfYear = 0;
        var loanAmounts=0;
        var intrestType1 = $scope.loanData.IntrestRate1;
        var intrestType2 = $scope.loanData.IntrestRate2;
        var intrestType3 = $scope.loanData.IntrestRate3;

        if ($scope.loanData.bankAmount1) {
            bankAmountOne = $scope.loanData.bankAmount1;

        }
        if ($scope.loanData.bankAmount2) {
            bankAmountTwo = $scope.loanData.bankAmount2;

        }
        if ($scope.loanData.bankAmount3) {
            bankAmountThree = $scope.loanData.bankAmount3;

        }

        if ($scope.loanData.relativeAmount1) {
            relativeAmountOne = $scope.loanData.relativeAmount1;

        }
        if ($scope.loanData.relativeAmount2) {
            relativeAmountTwo = $scope.loanData.relativeAmount2;

        }
        if ($scope.loanData.relativeAmount3) {
            relativeAmountThree = $scope.loanData.relativeAmount3;

        }



        if ($scope.loanData.bankIntrest1) {
            bankIntrestOne = ($scope.loanData.bankIntrest1 / 100) * bankAmountOne;
        }

        if ($scope.loanData.bankIntrest2) {
            bankIntrestTwo = ($scope.loanData.bankIntrest2 / 100) * bankAmountTwo;

        }

        if ($scope.loanData.bankIntrest3) {
            bankIntrestThree = ($scope.loanData.bankIntrest3 / 100) * bankAmountThree;

        }
        if (intrestType1 == '%') {
            relativeIntrestOne = ($scope.loanData.relativeIntrest1 / 100) * relativeAmountOne;

        } else if (intrestType1 == 'Rs') {

            relativeIntrestOne = ($scope.loanData.relativeIntrest1 * 12 / 100) * relativeAmountOne;
        }

        if (intrestType2 == '%') {
            relativeIntrestTwo = ($scope.loanData.relativeIntrest2 / 100) * relativeAmountTwo;

        } else if (intrestType2 == 'Rs') {

            relativeIntrestTwo = ($scope.loanData.relativeIntrest2 * 12 / 100) * relativeAmountTwo;
        }

        if (intrestType3 == '%') {
            relativeIntrestThree = ($scope.loanData.relativeIntrest3 / 100) * relativeAmountThree;

        } else if (intrestType3 == 'Rs') {

            relativeIntrestThree = ($scope.loanData.relativeIntrest3 * 12 / 100) * relativeAmountThree;
        }

        if ($scope.loanData.NoOfYear) {
            NoOfYear = $scope.loanData.NoOfYear;

        }
        if($scope.loanData.loanAmount)
        {
            loanAmounts=$scope.loanData.loanAmount;

        }
        $scope.loanData.TotalIntrest = bankIntrestOne + bankIntrestTwo + bankIntrestThree + relativeIntrestOne + relativeIntrestTwo + relativeIntrestThree;
        $scope.loanData.TotalIntrestAmounts = $scope.loanData.TotalIntrest * NoOfYear;
        $scope.loanData.totalLoanAmounts=loanAmounts+ $scope.loanData.TotalIntrestAmounts ;










    }


    if (localStorageService.get('loanAmountDatas') !== null) {
        $scope.loanData.totalLoanAmount = localStorageService.get('loanAmountDatas').loanAmountRequired;
        $scope.loanData.handCash = localStorageService.get('loanAmountDatas').handCash;
        $scope.loanData.loanAmount = localStorageService.get('loanAmountDatas').loanAmout;
        $scope.loanData.bankName1 = localStorageService.get('loanAmountDatas').bankName1;
        $scope.loanData.bankName2 = localStorageService.get('loanAmountDatas').bankName2;
        $scope.loanData.bankName3 = localStorageService.get('loanAmountDatas').bankName3;
        if (localStorageService.get('loanAmountDatas').bankAmount1 == 0) {
            $scope.loanData.bankAmount1 = '';

        } else {
            $scope.loanData.bankAmount1 = localStorageService.get('loanAmountDatas').bankAmount1;
        }
        if (localStorageService.get('loanAmountDatas').bankAmount2 == 0) {
            $scope.loanData.bankAmount2 = '';

        } else {
            $scope.loanData.bankAmount2 = localStorageService.get('loanAmountDatas').bankAmount2;
        }
        if (localStorageService.get('loanAmountDatas').bankAmount3 == 0) {
            $scope.loanData.bankAmount3 = '';

        } else {
            $scope.loanData.bankAmount3 = localStorageService.get('loanAmountDatas').bankAmount3;
        }

        if (localStorageService.get('loanAmountDatas').bankIntrest1 == 0) {
            $scope.loanData.bankIntrest1 = '';

        } else {
            $scope.loanData.bankIntrest1 = localStorageService.get('loanAmountDatas').bankIntrest1;
        }

        if (localStorageService.get('loanAmountDatas').bankIntrest2 == 0) {
            $scope.loanData.bankIntrest2 = '';

        } else {
            $scope.loanData.bankIntrest2 = localStorageService.get('loanAmountDatas').bankIntrest2;
        }


        if (localStorageService.get('loanAmountDatas').bankIntrest3 == 0) {
            $scope.loanData.bankIntrest3 = '';

        } else {
            $scope.loanData.bankIntrest3 = localStorageService.get('loanAmountDatas').bankIntrest3;
        }











        if (localStorageService.get('loanAmountDatas').relativeAmount1 == 0) {
            $scope.loanData.relativeAmount1 = '';

        } else {
            $scope.loanData.relativeAmount1 = localStorageService.get('loanAmountDatas').relativeAmount1;
        }
        if (localStorageService.get('loanAmountDatas').relativeAmount2 == 0) {
            $scope.loanData.relativeAmount2 = '';

        } else {
            $scope.loanData.relativeAmount2 = localStorageService.get('loanAmountDatas').relativeAmount2;
        }
        if (localStorageService.get('loanAmountDatas').relativeAmount3 == 0) {
            $scope.loanData.relativeAmount3 = '';

        } else {
            $scope.loanData.relativeAmount3 = localStorageService.get('loanAmountDatas').relativeAmount3;
        }

        if (localStorageService.get('loanAmountDatas').relativeIntrest1 == 0) {
            $scope.loanData.relativeIntrest1 = '';

        } else {
            console.log("asas");
            $scope.loanData.relativeIntrest1 = localStorageService.get('loanAmountDatas').relativeIntrest1;
        }

        if (localStorageService.get('loanAmountDatas').relativeIntrest2 == 0) {
            $scope.loanData.relativeIntrest2 = '';

        } else {
            $scope.loanData.relativeIntrest2 = localStorageService.get('loanAmountDatas').relativeIntrest2;
        }


        if (localStorageService.get('loanAmountDatas').relativeIntrest3 == 0) {
            $scope.loanData.relativeIntrest3 = '';

        } else {
            $scope.loanData.relativeIntrest3 = localStorageService.get('loanAmountDatas').relativeIntrest3;
        }










        $scope.loanData.relativeName1 = localStorageService.get('loanAmountDatas').relativeName1;
        $scope.loanData.relativeName2 = localStorageService.get('loanAmountDatas').relativeName2;
        $scope.loanData.relativeName3 = localStorageService.get('loanAmountDatas').relativeName3;






        $scope.loanData.IntrestRate1 = localStorageService.get('loanAmountDatas').intrestType1;
        $scope.loanData.IntrestRate2 = localStorageService.get('loanAmountDatas').intrestType2;
        $scope.loanData.IntrestRate3 = localStorageService.get('loanAmountDatas').intrestType3;

    }


    $scope.LoanSubmit = function(isValid) {

        if (isValid) {

            var loanAmount = $scope.loanData.loanAmount;
            var handCash = $scope.loanData.handCash;
            var loanAmoutRequired = $scope.loanData.totalLoanAmount;
            if (loanAmount < 0) {
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: "Loan amount shouldn't be negaitve"
                });

                return false;


            }








            /*source amount of bank*/
            var bankName1 = $scope.loanData.bankName1;
            var bankName2 = $scope.loanData.bankName2;
            var bankName3 = $scope.loanData.bankName3;


            var bankAmount1 = $scope.loanData.bankAmount1;

            var bankAmount2 = $scope.loanData.bankAmount2;
            var bankAmount3 = $scope.loanData.bankAmount3;


            var bankIntrest1 = $scope.loanData.bankIntrest1;


            var bankIntrest2 = $scope.loanData.bankIntrest2;
            var bankIntrest3 = $scope.loanData.bankIntrest3;

            if (bankAmount1 == undefined || bankAmount1 == null) {
                bankAmount1 = 0;

            }
            if (bankAmount2 == undefined || bankAmount2 == null) {
                bankAmount2 = 0;
            }
            if (bankAmount3 == undefined || bankAmount3 == null) {
                bankAmount3 = 0;
            }

            if (bankIntrest1 == undefined || bankIntrest1 == null) {
                bankIntrest1 = 0;
            }
            if (bankIntrest2 == undefined || bankIntrest2 == null) {
                bankIntrest2 = 0;
            }
            if (bankIntrest3 == undefined || bankIntrest3 == null) {
                bankIntrest3 = 0;
            }



            if (bankName1 != undefined || bankName1 != null) {
                if (bankAmount1 == 0 || bankIntrest1 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (bankName2 != undefined || bankName2 != null) {
                if (bankAmount2 == 0 || bankIntrest2 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (bankName3 != undefined || bankName3 != null) {
                if (bankAmount3 == 0 || bankIntrest3 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (bankAmount1 != 0) {
                if (bankName1 == undefined || bankIntrest1 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (bankAmount2 != 0) {
                if (bankName2 == undefined || bankIntrest2 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (bankAmount3 != 0) {
                if (bankName3 == undefined || bankIntrest3 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (bankIntrest1 != 0) {
                if (bankName1 == undefined || bankAmount1 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (bankIntrest2 != 0) {
                if (bankName2 == undefined || bankAmount2 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (bankIntrest3 != 0) {
                if (bankName3 == undefined || bankAmount3 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }




            var amountWithIntrest1 = bankAmount1 + (bankIntrest1 / 100) * bankAmount1;
            var amountWithIntrest2 = bankAmount2 + (bankIntrest2 / 100) * bankAmount2;

            var amountWithIntrest3 = bankAmount3 + (bankIntrest3 / 100) * bankAmount3;

            var totalBankLoan = amountWithIntrest1 + amountWithIntrest2 + amountWithIntrest3;

            /*source amount from others*/
            var relativeName1 = $scope.loanData.relativeName1;
            var relativeName2 = $scope.loanData.relativeName2;
            var relativeName3 = $scope.loanData.relativeName3;
            var relativeAmount1 = $scope.loanData.relativeAmount1;
            var relativeAmount2 = $scope.loanData.relativeAmount2;
            var relativeAmount3 = $scope.loanData.relativeAmount3;
            var relativeIntrest1 = $scope.loanData.relativeIntrest1;
            var relativeIntrest2 = $scope.loanData.relativeIntrest2;
            var relativeIntrest3 = $scope.loanData.relativeIntrest3;
            var totalIntrest = $scope.loanData.TotalIntrest;
            var noOfYears = $scope.loanData.NoOfYear;
            var totalIntrestAmount = $scope.loanData.TotalIntrestAmounts;
            var totalLoanAmounts = $scope.loanData.totalLoanAmounts;


            if (relativeAmount1 == undefined || relativeAmount1 == null) {
                relativeAmount1 = 0;
            }
            if (relativeAmount2 == undefined || relativeAmount2 == null) {
                relativeAmount2 = 0;
            }
            if (relativeAmount3 == undefined || relativeAmount3 == null) {
                relativeAmount3 = 0;
            }

            if (relativeIntrest1 == undefined || relativeIntrest1 == null) {
                relativeIntrest1 = 0;
            }
            if (relativeIntrest2 == undefined || relativeIntrest2 == null) {
                relativeIntrest2 = 0;
            }
            if (relativeIntrest3 == undefined || relativeIntrest3 == null) {
                relativeIntrest3 = 0;
            }





            if (relativeName1 != undefined || relativeName1 != null) {


                if (relativeAmount1 == 0 || relativeIntrest1 == 0) {

                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (relativeName2 != undefined || relativeName2 != null) {
                if (relativeAmount2 == 0 || relativeIntrest2 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (relativeName3 != undefined || relativeName3 != null) {
                if (relativeAmount3 == 0 || relativeIntrest3 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }



            if (relativeAmount1 != 0) {
                if (relativeName1 == undefined || relativeIntrest1 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (relativeAmount2 != 0) {
                if (relativeName2 == undefined || relativeIntrest2 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }

            if (relativeAmount3 != 0) {
                if (relativeName3 == undefined || relativeIntrest3 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }



            if (relativeIntrest1 != 0) {
                if (relativeName1 == undefined || relativeAmount1 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }
            if (relativeIntrest2 != 0) {
                if (relativeName2 == undefined || relativeAmount2 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }
            if (relativeIntrest3 != 0) {
                if (relativeName3 == undefined || relativeAmount3 == 0) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please fill the corresponding fileds.'
                    });

                    return false;

                }
            }



            var intrestType1 = $scope.loanData.IntrestRate1;
            var intrestType2 = $scope.loanData.IntrestRate2;
            var intrestType3 = $scope.loanData.IntrestRate3;









            if (intrestType1 == '%') {

                var amountWithRelativeIntrest1 = relativeAmount1 + (relativeIntrest1 / 100) * relativeAmount1;
                // var amountWithRelativeIntrest2 = relativeAmount2 + (relativeIntrest2 / 100) * relativeAmount2;
                // var amountWithRelativeIntrest3 = relativeAmount3 + (relativeIntrest3 / 100) * relativeAmount3;
            } else if (intrestType1 == 'Rs') {

                relativeIntrest1 = relativeIntrest1 * 12;
                // relativeIntrest2 = relativeIntrest2 * 12;
                // relativeIntrest3 = relativeIntrest3 * 12;
                var amountWithRelativeIntrest1 = relativeAmount1 + (relativeIntrest1 / 100) * relativeAmount1;
                // var amountWithRelativeIntrest2 = relativeAmount2 + (relativeIntrest2 / 100) * relativeAmount2;
                // var amountWithRelativeIntrest3 = relativeAmount3 + (relativeIntrest3 / 100) * relativeAmount3;
                relativeIntrest1 = relativeIntrest1 / 12;

            }

            if (intrestType2 == '%') {

                // var amountWithRelativeIntrest1 = relativeAmount1 + (relativeIntrest1 / 100) * relativeAmount1;
                var amountWithRelativeIntrest2 = relativeAmount2 + (relativeIntrest2 / 100) * relativeAmount2;
                // var amountWithRelativeIntrest3 = relativeAmount3 + (relativeIntrest3 / 100) * relativeAmount3;
            } else if (intrestType2 == 'Rs') {

                // relativeIntrest1 = relativeIntrest1 * 12;
                relativeIntrest2 = relativeIntrest2 * 12;
                // relativeIntrest3 = relativeIntrest3 * 12;
                // var amountWithRelativeIntrest1 = relativeAmount1 + (relativeIntrest1 / 100) * relativeAmount1;
                var amountWithRelativeIntrest2 = relativeAmount2 + (relativeIntrest2 / 100) * relativeAmount2;
                // var amountWithRelativeIntrest3 = relativeAmount3 + (relativeIntrest3 / 100) * relativeAmount3;

                relativeIntrest2 = relativeIntrest2 / 12;
            }

            if (intrestType3 == '%') {

                // var amountWithRelativeIntrest1 = relativeAmount1 + (relativeIntrest1 / 100) * relativeAmount1;
                // var amountWithRelativeIntrest2 = relativeAmount2 + (relativeIntrest2 / 100) * relativeAmount2;
                var amountWithRelativeIntrest3 = relativeAmount3 + (relativeIntrest3 / 100) * relativeAmount3;
            } else if (intrestType3 == 'Rs') {

                // relativeIntrest1 = relativeIntrest1 * 12;
                // relativeIntrest2 = relativeIntrest2 * 12;
                relativeIntrest3 = relativeIntrest3 * 12;
                // var amountWithRelativeIntrest1 = relativeAmount1 + (relativeIntrest1 / 100) * relativeAmount1;
                // var amountWithRelativeIntrest2 = relativeAmount2 + (relativeIntrest2 / 100) * relativeAmount2;
                var amountWithRelativeIntrest3 = relativeAmount3 + (relativeIntrest3 / 100) * relativeAmount3;
                relativeIntrest3 = relativeIntrest3 / 12;

            }
            if (amountWithRelativeIntrest1 == undefined || amountWithRelativeIntrest1 == null) {
                amountWithRelativeIntrest1 = 0;

            }
            if (amountWithRelativeIntrest2 == undefined || amountWithRelativeIntrest2 == null) {
                amountWithRelativeIntrest2 = 0;

            }
            if (amountWithRelativeIntrest3 == undefined || amountWithRelativeIntrest3 == null) {
                amountWithRelativeIntrest3 = 0;

            }

            if (relativeName1 != undefined || relativeAmount1 != 0 || relativeIntrest1 != 0) {

                if (intrestType1 == undefined || intrestType1 == null) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please select intrest type.'
                    });

                    return false;

                }

            }

            if (relativeName2 != undefined || relativeAmount2 != 0 || relativeIntrest2 != 0) {

                if (intrestType2 == undefined || intrestType2 == null) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please select intrest type.'
                    });

                    return false;

                }

            }
            if (relativeName3 != undefined || relativeAmount3 != 0 || relativeIntrest3 != 0) {

                if (intrestType3 == undefined || intrestType3 == null) {
                    $ionicPopup.alert({
                        title: '<i class="ion ion-alert"></i>',
                        template: 'Please select intrest type.'
                    });

                    return false;

                }

            }


            var totalRelativeLoan = amountWithRelativeIntrest1 + amountWithRelativeIntrest2 + amountWithRelativeIntrest3;









            if (((bankAmount1 + bankAmount2 + bankAmount3 + relativeAmount1 + relativeAmount2 + relativeAmount3) > loanAmount) || ((bankAmount1 + bankAmount2 + bankAmount3 + relativeAmount1 + relativeAmount2 + relativeAmount3) != loanAmount)) {
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'Sum of total loan taken must be equal to the loan amount.'
                });

                return false;
            }

            // if (relativeName1 != undefined || relativeName2 != undefined || relativeName3 != undefined || relativeAmount1 != 0 || relativeAmount2 != 0 || relativeAmount3 != 0 || relativeIntrest1 != 0 || relativeIntrest2 != 0 || relativeIntrest3 != 0) {

            //     if (intrestType == undefined || intrestType == null) {
            //         $ionicPopup.alert({
            //             title: '<i class="ion ion-alert"></i>',
            //             template: 'Please select intrest type.'
            //         });

            //         return false;

            //     }

            // }



            /**/
            var totalLoanToBePaid = totalBankLoan + totalRelativeLoan;

            var loanAmountDatas = {
                loanAmountRequired: loanAmoutRequired,
                handCash: handCash,
                loanAmout: loanAmount,
                bankName1: bankName1,
                bankName2: bankName2,
                bankName3: bankName3,
                bankAmount1: bankAmount1,
                bankAmount2: bankAmount2,
                bankAmount3: bankAmount3,
                bankIntrest1: bankIntrest1,
                bankIntrest2: bankIntrest2,
                bankIntrest3: bankIntrest3,
                relativeName1: relativeName1,
                relativeName2: relativeName2,
                relativeName3: relativeName3,
                relativeAmount1: relativeAmount1,
                relativeAmount2: relativeAmount2,
                relativeAmount3: relativeAmount3,
                relativeIntrest1: relativeIntrest1,
                relativeIntrest2: relativeIntrest2,
                relativeIntrest3: relativeIntrest3,
                intrestType1: intrestType1,
                intrestType2: intrestType2,
                intrestType3: intrestType3,
                totalLoanWithIntrest: totalLoanToBePaid,
                totalIntrest: totalIntrest,
                totalIntrestAmount: totalIntrestAmount,
                totalLoanAmounts: totalLoanAmounts

            }





            localStorageService.set('loanAmountDatas', loanAmountDatas);
            $state.transitionTo('app.dashboard', null, { 'reload': true });
        } else {
            $scope.submitted = true;
        }


    }



}
