var embassiesEnglish = [
    {email: 'info@neksa.org', website:'www.neksa.org',fax:'(+966) 11- 4640690/ 4651823',tel:'(+966) 11- 4611108/ 4645170',address:'Al Urubah Street (Near Sulymanieh Hotel, Al-Jouf Road) Riyadh 11693', post:'94384',country:'Saudi Arabia'}, 
    {email: 'eonkualalumpur@mofa.gov.np', website:'',fax:'03-2020 1894',tel:'03-2020 1898/03-2020 1899',address:'Wisma Paradise (Level 1, 3 and 9) No. 63 Jalan Ampang 50450 Kuala Lumpur', post:'',country:'Malaysia'}, 
    {email: 'nrmbdoha@gmail.com', website:'www.nembdoha.com',fax:'00974-44675680',tel:'00974-44675681, 00974-44675683',address:'Villa number: 13, Street Number 81042 Ibane bajaha', post:'23002',country:'Qatar'}, 
    {email: 'info@nepembku.org', website:'www.kuwait.mofa.gov.np',fax:'00965-25321628, 25321601',tel:'00965-25321603, 25321604',address:'Villa Number: 514, Street Number 13 Block Number: 8, jabriya', post:'94384',country:'Kuwait'}, 
    {email: 'info@nepalembassyuae.org', website:'www.nepalembassyuae.org',fax:'00 97126344469',tel:'00 97126344737',address:'Sector W 15-2, Villa Number: 13/1, Plot Number 40 Al Karama Street,Al Karama Area', post:'38282',country:'United Arab Emirates'},
    {email: 'eonmuscat@mofa.gov.np', website:'',fax:'968 24696772',tel:'968 24696177',address:'Shatti Al-Qurum, Villa-2563, Way 2834', post:'517, PC 116',country:'Oman'}
];

var embassiesNepali = [
    {email: 'info@neksa.org', website:'www.neksa.org',fax:'(+९६६)११-४६४०६९०/४६५१८२३',tel:'(+९६६)११-४६१११०८/४६४५१७० ',address:'अल उरुबह सडक (सुल्य्मनिएह होटल नजिकै, अल- जोउफ़ रोड) रियाद ११६९३', post:'९४३८४',country:'साउदी अरब'}, 
    {email: 'eonkualalumpur@mofa.gov.np', website:'',fax:'०३–२०२० १८९४',tel:'०३-२०२० १८९८ /०३-२०२० १८९९',address:'विस्मा प्याराडाईज (स्तर १,३  र ९) न. ६३ जालान अम्पंग ५०४५० क्वालालम्पुर', post:'',country:'मलेशिया'}, 
    {email: 'nrmbdoha@gmail.com', website:'www.nembdoha.com',fax:'००९७४-४४६७५६८०',tel:'००९७४-४४६७५६८१, ००९७४-४४६७५६८३',address:'भिल्ला नम्बर: १३, स्ट्रिट् नम्बर ८१०४२ इबने बजाह', post:'२३००२',country:'कतार'}, 
    {email: 'info@nepembku.org', website:'www.kuwait.mofa.gov.np',fax:'००९६५-२५३२१६२८, २५३२१६०१',tel:'००९६५-२५३२१६०३, २५३२१६०४',address:'भिल्ला नम्बर : ५१४, स्ट्रिट्  नम्बर १३ ब्लक नम्बर : ८,  जाबिंया', post:'',country:'कुवेत'}, 
    {email: 'info@nepalembassyuae.org', website:'www.nepalembassyuae.org',fax:'००९७१-२६३४४४६९',tel:'००९७१-२६३४४७३७',address:'सेक्टर डब्लु १५-२, भिल्ला नम्बर: १३/१, प्लट नम्बर  ४० अल करामा स्ट्रिट्, अल करामा एरिया', post:'३८२८२',country:'संयुक्त अरब एमिरेट्स'},
    {email: 'eonmuscat@mofa.gov.np', website:'',fax:'९६८ २४६९६७७२',tel:'९६८ २४६९६१७७',address:'शत्ती एआई–क्युरुम, भिल्ला–२५६३, वे २८३४', post:'५१७, पिसी ११६',country:'ओमान'}
];

function getEmbessies(from,to,lang){
    if(lang === 'English') {
        return embassiesEnglish.slice(from,to);
    } else {
        return embassiesNepali.slice(from,to);
    }
}