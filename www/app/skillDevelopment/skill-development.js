angular.module('starter').controller('skillDevelopment', ['$scope', '$rootScope', '$ionicModal', '$timeout', '$rootScope', '$http', '$ionicPopup', '$state', 'localStorageService', skillDevelopment]);

function skillDevelopment($scope, $rootScope, $ionicModal, $timeout, $rootScope, $http, $ionicPopup, $state, localStorageService) {
    $scope.selectedLanguage = $rootScope.language;
    $scope.selectedLanguage = "English";
    $scope.trainingCenters = '';
    $scope.readonly = false;
    $scope.lang = $scope.selectedLanguage;
    if ($scope.selectedLanguage === 'English')
        $scope.trainingCenterDetail = gettrainingCenter('englishAll');
    else
        $scope.trainingCenterDetail = gettrainingCenter('nepaliAll');
    //Training Center Modal And Category
    $ionicModal.fromTemplateUrl('app/skillDevelopment/training-centers-Category.html', {
            scope: $scope
        })
        .then(function(modal) {
            $scope.skillDevelopmentCategories = modal;
        });
    $scope.closeModalCategory = function(name) {
        $scope.skillDevelopmentCategories.hide();
        if ($scope.skillDevelopmentCategoriesData.selectedTrainingCenterCategories)

            $scope.trainingCenters = gettrainingCenter(name);
        $rootScope.langg = name;



    };
    $scope.skillDevelopmentCategoriesData = {
        selectedTrainingCenterCategories: null,
        trainingFee: null,
        additionalCost: null
    };


    //Traning Center Modal And Lisiting
    $ionicModal.fromTemplateUrl('app/skillDevelopment/training-centers.html', {
            scope: $scope
        })
        .then(function(modal) {
            $scope.skillDevelopment = modal;
        });
    $scope.closeModal = function() {
        $scope.skillDevelopment.hide();
    };

    if ($scope.selectedLanguage === 'Nepali') {
        $scope.trainingCenterCategories = [
            { name: 'निर्माण क्षेत्र', value: 'contructionNepali' },
            { name: 'होटल व्यवस्थापन क्षेत्र', value: 'hotelNepali' },
            { name: 'टेलरिङ्ग क्षेत्र', value: 'tailoringNepali' },
            { name: 'सुरक्षा गार्ड', value: 'securityNepali' }
        ];
    } else {
        $scope.trainingCenterCategories = [
            { name: 'Construction Zone', value: 'contructionEnglish' },
            { name: 'Hotel Management Area', value: 'hotelEnglish' },
            { name: 'Tailoring Area', value: 'tailoringEnglish' },
            { name: 'Security Guard', value: 'securityEnglish' }
        ];
    }


    $scope.skillDevelopmentData = {
        selectedTrainingCenter: null,
        trainingFee: null,
        additionalCost: null
    };




    if (localStorageService.get('skillDevelopment') !== null) {
        $scope.skillDevelopmentCategoriesData.selectedTrainingCenterCategories = localStorageService.get('skillDevelopment').selectedTrainingCategory;
        $scope.skillDevelopmentData.selectedTrainingCenter = localStorageService.get('skillDevelopment').selectedTrainingCenter;
        $scope.trainingCenters = gettrainingCenter(localStorageService.get('skillDevelopment').categoryLang);
        $scope.skillDevelopmentData.trainingFee = localStorageService.get('skillDevelopment').trainingFee;
        $scope.skillDevelopmentData.additionalCost = localStorageService.get('skillDevelopment').additionalCost;



    }


    if (localStorageService.get('BulkAmountData') != null) {

        if (localStorageService.get('BulkAmountData').SkillDevelopment != undefined && localStorageService.get('BulkAmountData').SkillDevelopment != false) {

            $scope.readonly = true;
            $scope.skillDevelopmentData.trainingFee = 0;
            if (localStorageService.get('skillDevelopment') !== null) {
                $scope.skillDevelopmentCategoriesData.selectedTrainingCenterCategories = localStorageService.get('skillDevelopment').selectedTrainingCategory;
                $scope.skillDevelopmentData.selectedTrainingCenter = localStorageService.get('skillDevelopment').selectedTrainingCenter;
                $scope.trainingCenters = gettrainingCenter(localStorageService.get('skillDevelopment').categoryLang);

                $scope.skillDevelopmentData.additionalCost = localStorageService.get('skillDevelopment').additionalCost;
            }

        }

    } else {

        if (localStorageService.get('skillDevelopment') !== null) {
            $scope.skillDevelopmentCategoriesData.selectedTrainingCenterCategories = localStorageService.get('skillDevelopment').selectedTrainingCategory;
            $scope.skillDevelopmentData.selectedTrainingCenter = localStorageService.get('skillDevelopment').selectedTrainingCenter;
            $scope.trainingCenters = gettrainingCenter(localStorageService.get('skillDevelopment').categoryLang);
            $scope.skillDevelopmentData.trainingFee = localStorageService.get('skillDevelopment').trainingFee;
            $scope.skillDevelopmentData.additionalCost = localStorageService.get('skillDevelopment').additionalCost;


        }


    }

    $scope.SkillDevelopmentSubmit = function(isValid) {
        if (isValid) {
            var skillDevelopmentCategory = $scope.skillDevelopmentCategoriesData.selectedTrainingCenterCategories;
            var trainningCenter = $scope.skillDevelopmentData.selectedTrainingCenter;

            // if (skillDevelopmentCategory == null || skillDevelopmentCategory == '') {
            //     $ionicPopup.alert({
            //         title: '<i class="ion ion-alert"></i>',
            //         template: 'Please select a category.'
            //     });
            // } else if (trainningCenter == null || trainningCenter == '') {
            //     $ionicPopup.alert({
            //         title: '<i class="ion ion-alert"></i>',
            //         template: 'Please select trainning center.'
            //     });

            // } else {
                var trainingFee = $scope.skillDevelopmentData.trainingFee;
                var additionalCost = $scope.skillDevelopmentData.additionalCost;
                var TotalCost = trainingFee + additionalCost;


                var skillDevelopmentData = {
                    selectedTrainingCategory: $scope.skillDevelopmentCategoriesData.selectedTrainingCenterCategories,
                    categoryLang: $rootScope.langg,
                    selectedTrainingCenter: $scope.skillDevelopmentData.selectedTrainingCenter,
                    trainingFee: $scope.skillDevelopmentData.trainingFee,
                    additionalCost: $scope.skillDevelopmentData.additionalCost,
                    TotalCost: TotalCost

                }


                localStorageService.set('skillDevelopment', skillDevelopmentData);
                $state.transitionTo('app.dashboard', null, { 'reload': true });


            // }



        } else {
            $scope.submitted = true;
        }


    }





}
