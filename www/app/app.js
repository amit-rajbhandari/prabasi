// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova','LocalStorageModule'])





.run(function($ionicPlatform, $cordovaStatusbar, $rootScope,$state, $stateParams) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.cordova) {
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
            $cordovaStatusbar.overlaysWebView(true);
            $cordovaStatusbar.styleHex('#1590c1');
        }
        //$rootScope.$stateParams = $stateParams;
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $cordovaInAppBrowserProvider) {
    
    $stateProvider

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'app/menu.html',
        controller: 'defaultController',
        cache: false

    })
    
    .state('language-selection', {
        url: '/language-selection',
        templateUrl: 'app/setupWizard/language-selection.html',
        controller: 'languageSelection'
    })
    
    .state('phone-number-registration', {
        url: '/phone-number-registration',
        templateUrl: 'app/setupWizard/phone-number-registration.html',
        controller: 'phoneRegistration',
        cache: false
    })
    
    .state('confirmation-code', {
        url: '/confirmation-code',
        templateUrl: 'app/setupWizard/confirmation-code.html',
        controller: 'setupWizard',
        cache: false
    })
    
    .state('registration-form', {
        url: '/registration-form',
        templateUrl: 'app/setupWizard/registration-form.html',
        controller: 'userRegistration',
        cache: false
    })
    
    .state('session-listing', {
        url: '/session-listing',
        templateUrl: 'app/session/session-listing.html',
        controller: 'session',
        cache: false
    })
    
    .state('country-selection', {
        url: '/country-selection',
        templateUrl: 'app/countrySelection/country-selection.html',
        controller: 'countrySelection',
        cache: false
    })
    
    .state('select-features', {
        url: '/select-features',
        templateUrl: 'app/select-features.html',
        controller: 'defaultController',
        cache: false
    })
  
    .state('app.dashboard', {
        url: '/dashboard',
        cache: false,

        views: {
          'menuContent': {
            templateUrl: 'app/dashboard.html',
            controller: 'defaultController'
             
          }
        }
    })
    
    .state('app.while-abroad', {
        url: '/while-abroad',
        views: {
          'menuContent': {
            templateUrl: 'app/while-abroad.html',
            controller: 'defaultController',
             cache: false
          }
        }
    })
    
    .state('app.pre-departure', {
        url: '/pre-departure',
        views: {
          'menuContent': {
            templateUrl: 'app/pre-departure.html',
            controller: 'defaultController',
             cache: false
          }
        }
    })

    .state('app.passport', {
        url: '/passport',
        cache:false,
        views: {
          'menuContent': {
            templateUrl: 'app/passport/passport.html',
            controller:'passportController'
            
          }
        }
    })
    
    .state('app.passport-info', {
        url: '/passport-info',
        views: {
          'menuContent': {
            templateUrl: 'app/passport/passport-info.html'
          }
        }
    })
    
    .state('app.passport-download-list', {
        url: '/passport-download-list',
        views: {
          'menuContent': {
            templateUrl: 'app/passport/passport-download-list.html'
          }
        }
    })
    
    .state('app.visa-request', {
        url: '/visa-request',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'app/visa-request.html',
            controller:'defaultController'


          }
        }
    })
    
    .state('app.health-checkups', {
        url: '/health-checkups',
        views: {
          'menuContent': {
            templateUrl: 'app/healthCheckups/health-checkups.html',
            controller: 'healthCheckups',
            cache: false
          }
        }
    })
    
    .state('app.health-center-details', {
        url: '/health-center-details',
        views: {
          'menuContent': {
            templateUrl: 'app/healthCheckups/health-center-details.html',
            controller: 'healthCheckups',
            cache: false
          }
        }
    })
    
    .state('app.manpower-agency', {
        url: '/manpower-agency',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'app/manpowerAgency/manpower-agency.html',
            controller: 'manpowerAgency'
            
          }
        }
    })
    .state('app.manpower-detail', {
        url: '/manpower-detail',
        views: {
          'menuContent': {
            templateUrl: 'app/manpowerAgency/manpower-detail.html',
            controller: 'manpowerAgency',
            cache: false
          }
        }
    })
    
    .state('app.loan', {
        url: '/loan',
        cache:false,
        views: {
          'menuContent': {
            templateUrl: 'app/loan.html',
            controller:'loanController'
          }
        }
    })
    
    .state('app.air-tickets', {
        url: '/air-tickets',
        cache:false,
        views: {
          'menuContent': {
            templateUrl: 'app/air-tickets.html',
            controller:'defaultController'
          }
        }
    })
    
    .state('app.insurance', {
        url: '/insurance',
        cache:false,
        views: {
          'menuContent': {
            templateUrl: 'app/insurance/insurance.html',
            controller:'insuranceController'
          }
        }
    })
    
    .state('app.compensation', {
        url: '/compensation',
        views: {
          'menuContent': {
            templateUrl: 'app/insurance/compensation.html'
          }
        }
    })
    
    .state('app.labor-permit', {
        url: '/labor-permit',
        views: {
          'menuContent': {
            templateUrl: 'app/labor-permit.html'
          }
        }
    })
    
    .state('app.welfare-fund', {
        url: '/welfare-fund',
        cache:false,
        views: {
          'menuContent': {
            templateUrl: 'app/welfare-fund.html',
            controller:'defaultController'
    
          }
        }
    })
    
    .state('app.pre-orientation-training', {
        url: '/pre-orientation-training',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'app/preOrientationTraining/pre-orientation-training.html',
            controller: 'preOrientationTraining'
          
          }
        }
    })
    .state('app.pre-orientation-centers-detal', {
        url: '/pre-orientation-centers-detal',
        views: {
          'menuContent': {
            templateUrl: 'app/preOrientationTraining/pre-orientation-centers-detal.html',
            controller: 'preOrientationTraining',
            cache: false
          }
        }
    })
    
    .state('app.contract', {
        url: '/contract',
        views: {
          'menuContent': {
            templateUrl: 'app/contract/contract.html',
            controller: 'employeeContract',
            cache: false
          }
        }
    })
    .state('app.contract-info', {
        url: '/contract-info',
        views: {
          'menuContent': {
            templateUrl: 'app/contract/contract-info.html',
            controller: 'employeeContract',
            cache: false
          }
        }
    })
    .state('app.contract-content', {
        url: '/contract-content',
        views: {
          'menuContent': {
            templateUrl: 'app/contract/contract-content.html',
            controller: 'employeeContract',
            cache: false
          }
        }
    })
    .state('app.skill-development', {
        url: '/skill-development',
        views: {
          'menuContent': {
            templateUrl: 'app/skillDevelopment/skill-development.html',
            controller: 'skillDevelopment',
            cache: false
          }
        }
    })
    .state('app.skill-development-training-center-detail', {
        url: '/skill-development-training-center-detail',
        views: {
          'menuContent': {
            templateUrl: 'app/skillDevelopment/skill-development-training-center-detail.html',
            controller: 'skillDevelopment',
            cache: false
          }
        }
    })
    
    .state('app.financial-analysis', {
        url: '/financial-analysis',
        cache:false,
        views: {
          'menuContent': {
            templateUrl: 'app/financial-analysis.html',
            controller:'defaultController'
          }
        }
    })
    
    .state('app.bulk-amount', {
        url: '/bulk-amount',
        cache:false,
        views: {
          'menuContent': {
            templateUrl: 'app/bulkAmount/bulk-amount.html',
            controller:'bulkAmountController'
          }
        }
    })
    
    .state('app.blocked-companies', {
        url: '/blocked-companies',
        views: {
          'menuContent': {
            templateUrl: 'app/blockedCompany/blocked-companies.html',
            controller: 'blockedCompany',
            cache: false
          }
        }
    })
    
    .state('app.embassies', {
        url: '/embassies',
        views: {
          'menuContent': {
            templateUrl: 'app/embassies/embassies.html',
            controller: 'embassies',
            cache: false
          }
        }
    });
    
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/language-selection');
});