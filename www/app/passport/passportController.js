angular.module('starter').controller('passportController', ['$scope', '$state', '$rootScope', 'localStorageService', '$window', function($scope, $state, $rootScope, localStorageService, $window) {
    $scope.passportData = {};
    $scope.readonly = false;


    if (localStorageService.get('PassportCost') !== null) {
        $scope.passportData.passportCost = localStorageService.get('PassportCost').passportCost;
        $scope.passportData.additionalCost = localStorageService.get('PassportCost').extraCost;
    }


    if (localStorageService.get('BulkAmountData') != null) {
        if (localStorageService.get('BulkAmountData').Passport != undefined && localStorageService.get('BulkAmountData').Passport != false) {
            $scope.readonly = true;


            $scope.passportData.passportCost = 0;

            if (localStorageService.get('PassportCost') !== null) {

                $scope.passportData.additionalCost = localStorageService.get('PassportCost').extraCost;


            }

        } else {

            if (localStorageService.get('PassportCost') !== null) {

                $scope.passportData.passportCost = localStorageService.get('PassportCost').passportCost;
                $scope.passportData.additionalCost = localStorageService.get('PassportCost').extraCost;

            }



        }


    }



    $scope.PassportSubmit = function(isValid) {
        if (isValid) {
            var passportCost = $scope.passportData.passportCost;
            var extraCost = $scope.passportData.additionalCost;
            var TotalCost = passportCost + extraCost;
            var passportCostData = {
                passportCost: passportCost,
                extraCost: extraCost,
                TotalCost: TotalCost

            }
            localStorageService.set('PassportCost', passportCostData);
            // $rootScope.PassportTotalCost = localStorageService.get('PassportCost').TotalCost;
            $state.transitionTo('app.dashboard', null, { 'reload': true });


        } else {
            $scope.submitted = true;
        }
    }

    $scope.resetCost = function() {
        // localStorageService.remove('PassportCost');
        //  $scope.passportData.passportCost = '';
        //  $scope.passportData.additionalCost = '';






        // $state.go($state.current,null, {reload: true, inherit: false});
        // $window.location.href = '#/app/passport';

    }









}])
