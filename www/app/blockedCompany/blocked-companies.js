angular.module('starter').controller('blockedCompany',['$scope', '$rootScope', '$http',blockedCompany]);
function blockedCompany($scope, $rootScope, $http){
    $scope.selectedLanguage = $rootScope.language;

    if($scope.selectedLanguage === "Nepali") {
        var httpRequest = $http({
            url: 'app/blockedCompany/company-list-nepali.json'

        }).success(function(data) {
            $scope.blockedCompanies = data;
        });
    } else {
        var httpRequest = $http({
            url: 'app/blockedCompany/company-list-english.json'

        }).success(function(data) {
            $scope.blockedCompanies = data;
        });
    }
}