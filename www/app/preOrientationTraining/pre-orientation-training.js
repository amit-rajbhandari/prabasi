angular.module('starter').controller('preOrientationTraining', ['$scope', '$ionicModal', '$timeout', '$rootScope', '$ionicPopup', 'localStorageService', '$state', preOrientationTraining]);

function preOrientationTraining($scope, $ionicModal, $timeout, $rootScope, $ionicPopup, localStorageService, $state) {

    // $scope.selectedLanguage = $rootScope.language;
    $scope.selectedLanguage = "English";
    $scope.lang = $scope.selectedLanguage;
    $scope.relatedListings = [];
    $scope.readonly = false;
    var limit = 50;
    $scope.from = 0;
    $scope.to = $scope.from + limit;
    //Manpower Center Modal
    $ionicModal.fromTemplateUrl('app/preOrientationTraining/pre-orientation-training-related-list.html', {
        scope: $scope
    })

    .then(function(modal) {
        $scope.preOrientationRelatedList = modal;
    });

    $scope.closeModal = function() {
        $scope.preOrientationRelatedList.hide();
    };

    $scope.preOrientationData = {
        selectedrelateListing: null,
        preOrientationFee: null,
        preOrientationAdditionalCost: null
    };

    function init() {
        var result = getCenters($scope.from, $scope.to, $scope.lang);
        for (var i = 0; i < result.length; i++) {
            $scope.relatedListings.push(result[i]);
        }
    }

    init();
    $scope.loadmoreCenter = function() {
        $scope.from = $scope.to;
        $scope.to = $scope.from + limit;
        var result = getCenters($scope.from, $scope.to, $scope.lang);
        for (var i = 0; i < result.length; i++) {
            $scope.relatedListings.push(result[i]);
        }
    };



    if (localStorageService.get('PreOrientationData') !== null) {
        $scope.preOrientationData.selectedrelateListing = localStorageService.get('PreOrientationData').selectedTrainningCenter;
        $scope.preOrientationData.trainningCost = localStorageService.get('PreOrientationData').trainningCost;
        $scope.preOrientationData.additionalCost = localStorageService.get('PreOrientationData').additionalCost;


    }

    if (localStorageService.get('BulkAmountData') != null) {

        if (localStorageService.get('BulkAmountData').PreOrientationTrainning != undefined && localStorageService.get('BulkAmountData').PreOrientationTrainning != false) {
            $scope.readonly = true;
            $scope.preOrientationData.trainningCost = 0;
            if (localStorageService.get('PreOrientationData') !== null) {
                $scope.preOrientationData.selectedrelateListing = localStorageService.get('PreOrientationData').selectedTrainningCenter;
                $scope.preOrientationData.additionalCost = localStorageService.get('PreOrientationData').additionalCost;

            }



        }


    } else {
        if (localStorageService.get('PreOrientationData') !== null) {
            $scope.preOrientationData.selectedrelateListing = localStorageService.get('PreOrientationData').selectedTrainningCenter;
            $scope.preOrientationData.trainningCost = localStorageService.get('PreOrientationData').trainningCost;

            $scope.preOrientationData.additionalCost = localStorageService.get('PreOrientationData').additionalCost;

        }

    }

    $scope.TrainningSubmit = function(isValid) {



        if (isValid) {
            var trainningCenter = $scope.preOrientationData.selectedrelateListing;
            if (trainningCenter == null || trainningCenter == '') {
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'Please select trainning center.'
                });

            } else {
                var trainningCost = $scope.preOrientationData.trainningCost;
                var additionalCost = $scope.preOrientationData.additionalCost;
                var TotalCost = trainningCost + additionalCost;


                var preOrientationData = {
                    selectedTrainningCenter: trainningCenter,
                    trainningCost: trainningCost,
                    additionalCost: additionalCost,
                    TotalCost: TotalCost,
                    isRefund: $scope.preOrientationData.refund

                }
                localStorageService.set('PreOrientationData', preOrientationData);

                $state.transitionTo('app.dashboard', null, { 'reload': true });


            }

        } else {
            $scope.submitted = true;
        }



    }



}
