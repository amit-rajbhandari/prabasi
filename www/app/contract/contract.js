angular.module('starter').controller('employeeContract', ['$scope', '$rootScope', '$location', '$ionicPopup', '$window', 'localStorageService', '$state', '$timeout', employeeContract]);

function employeeContract($scope, $rootScope, $location, $ionicPopup, $window, localStorageService, $state, $timeout) {
    // $scope.selectedLanguage = $rootScope.language;
    $scope.selectedLanguage = "English";
    $scope.contractData = {};
    $scope.disabled = true;



    $scope.onChange = function() {
        var empName = angular.element(document.querySelector('#employeeName')).val();


        var probation = angular.element(document.querySelector('#probation')).val();
        var empData = {
            employeeName: empName,
            probation: probation
        }
        localStorageService.set('employeeData', empData);
        if (empName.length > 0 && probation.length > 0) {
            $scope.disabled = false;
        } else {
            $scope.disabled = true;

        }
    }



    $scope.GoTo = function(state) {
        if (state != undefined && state.length > 0) {
            $window.location.hash = state;
        }
    }




    var countryName = localStorageService.get('CountrySelection').country;
    if (countryName == 'United Arab Emirates') {
        $scope.priceSign = 'Dirham';
    } else if (countryName == 'Saudi Arabia') {
        $scope.priceSign = 'Riyal';

    } else if (countryName == 'Qatar') {
        $scope.priceSign = 'Riyal';

    } else if (countryName == 'Malyasia') {
        $scope.priceSign = 'Ringgit';

    } else if (countryName == 'kuwait') {
        $scope.priceSign = 'Dinar';

    } else {
        $scope.priceSign = 'Omani';

    }






    $scope.onChangePrice = function() {
        var currency = 0;

        var country = localStorageService.get('CountrySelection').country;
        if (country == 'United Arab Emirates') {
            var price = localStorageService.get('CurrencyDoller').AED;
            currency = price * 106.65;
        } else if (country == 'United Arab Emirates') {
            var price = localStorageService.get('CurrencyDoller').AED;
            currency = price * 106.65;

        } else if (country == 'Saudi Arabia') {
            var price = localStorageService.get('CurrencyDoller').SAR;
            currency = price * 106.65;

        } else if (country == 'Qatar') {
            var price = localStorageService.get('CurrencyDoller').QAR;
            currency = price * 106.65;

        } else if (country == 'Malyasia') {
            var price = localStorageService.get('CurrencyDoller').MYR;
            currency = price * 106.65;

        } else if (country == 'kuwait') {
            var price = localStorageService.get('CurrencyDoller').KWD;
            currency = price * 106.65;

        } else {
            var price = localStorageService.get('CurrencyDoller').OMR;
            currency = price * 106.65;

        }
        var monthlyamount = $scope.contractData.foreignMonthlyIncome;
        var houseexpense = $scope.contractData.foreignHousingExpense;
        var foodExpense = $scope.contractData.foreignFoodExpense;
        var phoneExpense = $scope.contractData.foreignPhoneExpense;
        var transportExpense = $scope.contractData.foreignTransportExpense;
        var otherExpense = $scope.contractData.foreignOtherExpense;
        var periodContract = $scope.contractData.contractPeriod;

        $scope.contractData.nepaliMonthlyIncome = Math.round(monthlyamount * currency);
        $scope.contractData.nepaliHousingExpense = Math.round(houseexpense * currency);
        $scope.contractData.nepaliFoodExpense = Math.round(foodExpense * currency);
        $scope.contractData.nepaliPhoneExpense = Math.round(phoneExpense * currency);
        $scope.contractData.nepaliTransportExpense = Math.round(transportExpense * currency);
        if (otherExpense !== null) {
            $scope.contractData.nepaliOtherExpense = Math.round(otherExpense * currency);

        } else {
            $scope.contractData.nepaliOtherExpense = null;


        }

        console.log(periodContract)
        if (periodContract != undefined) {
            $scope.contractData.foreignTotalIncomeContract = Math.round(monthlyamount) * periodContract;
            $scope.contractData.nepaliTotalIncomeContract = Math.round($scope.contractData.nepaliMonthlyIncome) * periodContract;
            console.log("uforeignIncome", $scope.contractData.foreignTotalIncomeContract);
            console.log("uforeignTotalExpense", $scope.contractData.foreignTotalExpenseContract);
        } else {
            $scope.contractData.foreignTotalIncomeContract = Math.round(monthlyamount);
            $scope.contractData.nepaliTotalIncomeContract = Math.round($scope.contractData.nepaliMonthlyIncome);
            console.log("foreignIncome", $scope.contractData.foreignTotalIncomeContract);
            console.log("foreignTotalExpense", $scope.contractData.foreignTotalExpenseContract);


        }

        var totalExpense = 0;
        var totalIncome = 0;


























        if (houseexpense && foodExpense && phoneExpense && transportExpense && otherExpense != null) {
            $scope.contractData.foreignTotalExpense = Math.round(houseexpense + foodExpense + phoneExpense + transportExpense + otherExpense);
            $scope.contractData.nepaliTotalExpense = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense);
            if (periodContract != undefined) {
                $scope.contractData.foreignTotalExpenseContract = Math.round(houseexpense + foodExpense + phoneExpense + transportExpense + otherExpense) * periodContract;
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense) * periodContract;

            } else {
                $scope.contractData.foreignTotalExpenseContract = Math.round(houseexpense + foodExpense + phoneExpense + transportExpense + otherExpense);
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense);
            }



        } else if (houseexpense && foodExpense && phoneExpense && transportExpense && otherExpense == null) {
            $scope.contractData.foreignTotalExpense = Math.round(houseexpense + foodExpense + phoneExpense + transportExpense);
            $scope.contractData.nepaliTotalExpense = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense);

            if (periodContract != undefined) {

                $scope.contractData.foreignTotalExpenseContract = Math.round(houseexpense + foodExpense + phoneExpense + transportExpense) * periodContract;
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense) * periodContract;
            } else {
                $scope.contractData.foreignTotalExpenseContract = Math.round(houseexpense + foodExpense + phoneExpense + transportExpense);
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense);

            }
        } else if (houseexpense == undefined && foodExpense && phoneExpense && transportExpense && otherExpense != null) {

            $scope.contractData.foreignTotalExpense = Math.round(foodExpense + phoneExpense + transportExpense + otherExpense);
            $scope.contractData.nepaliTotalExpense = Math.round($scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense);

            if (periodContract != undefined) {
                $scope.contractData.foreignTotalExpenseContract = Math.round(foodExpense + phoneExpense + transportExpense + otherExpense) * periodContract;
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense) * periodContract;
            } else {
                $scope.contractData.foreignTotalExpenseContract = Math.round(foodExpense + phoneExpense + transportExpense + otherExpense);
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense);

            }
        } else if (houseexpense == undefined && foodExpense && phoneExpense && transportExpense && otherExpense == null) {

            $scope.contractData.foreignTotalExpense = Math.round(foodExpense + phoneExpense + transportExpense);
            $scope.contractData.nepaliTotalExpense = Math.round($scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense);

            if (periodContract != undefined) {
                $scope.contractData.foreignTotalExpenseContract = Math.round(foodExpense + phoneExpense + transportExpense) * periodContract;
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense) * periodContract;
            } else {
                $scope.contractData.foreignTotalExpenseContract = Math.round(foodExpense + phoneExpense + transportExpense);
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliFoodExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense);

            }
        } else if (houseexpense && foodExpense == undefined && phoneExpense && transportExpense && otherExpense != null) {

            $scope.contractData.foreignTotalExpense = Math.round(houseexpense + phoneExpense + transportExpense + otherExpense);
            $scope.contractData.nepaliTotalExpense = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense);

            if (periodContract != undefined) {
                $scope.contractData.foreignTotalExpenseContract = Math.round(houseexpense + phoneExpense + transportExpense + otherExpense) * periodContract;
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense) * periodContract;
            } else {
                $scope.contractData.foreignTotalExpenseContract = Math.round(houseexpense + phoneExpense + transportExpense + otherExpense);
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense);

            }
        } else if (houseexpense && foodExpense == undefined && phoneExpense && transportExpense && otherExpense == null) {

            $scope.contractData.foreignTotalExpense = Math.round(houseexpense + phoneExpense + transportExpense);
            $scope.contractData.nepaliTotalExpense = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense);

            if (periodContract != undefined) {
                $scope.contractData.foreignTotalExpenseContract = Math.round(houseexpense + phoneExpense + transportExpense) * periodContract;
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense) * periodContract;
            } else {
                $scope.contractData.foreignTotalExpenseContract = Math.round(houseexpense + phoneExpense + transportExpense);
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliHousingExpense + $scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense);

            }
        } else if (houseexpense == undefined && foodExpense == undefined && phoneExpense && transportExpense && otherExpense != null) {

            $scope.contractData.foreignTotalExpense = Math.round(phoneExpense + transportExpense + otherExpense);
            $scope.contractData.nepaliTotalExpense = Math.round($scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense);

            if (periodContract != undefined) {
                $scope.contractData.foreignTotalExpenseContract = Math.round(phoneExpense + transportExpense + otherExpense) * periodContract;
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense) * periodContract;
            } else {
                $scope.contractData.foreignTotalExpenseContract = Math.round(phoneExpense + transportExpense + otherExpense);
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense + $scope.contractData.nepaliOtherExpense);

            }
        } else if (houseexpense == undefined && foodExpense == undefined && phoneExpense && transportExpense && otherExpense == null) {
            $scope.contractData.foreignTotalExpense = Math.round(phoneExpense + transportExpense);
            $scope.contractData.nepaliTotalExpense = Math.round($scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense);

            if (periodContract != undefined) {
                $scope.contractData.foreignTotalExpenseContract = Math.round(phoneExpense + transportExpense) * periodContract;
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense) * periodContract;
            } else {
                $scope.contractData.foreignTotalExpenseContract = Math.round(phoneExpense + transportExpense);
                $scope.contractData.nepaliTotalExpenseContract = Math.round($scope.contractData.nepaliPhoneExpense + $scope.contractData.nepaliTransportExpense);

            }
        }



        $scope.contractData.foreignTotalIncomes = $scope.contractData.foreignTotalIncomeContract - $scope.contractData.foreignTotalExpenseContract;

        $scope.contractData.nepaliTotalIncomes = $scope.contractData.nepaliTotalIncomeContract - $scope.contractData.nepaliTotalExpenseContract;


    }




    $scope.ContractSubmit = function(isValid) {

        if (isValid) {
            var nepaliIncome = $scope.contractData.nepaliMonthlyIncome;
            var contractPeroid = $scope.contractData.contractPeriod;

            var contractDatas = {
                employeeName: localStorageService.get('employeeData').employeeName,
                probation: localStorageService.get('employeeData').probation,
                foreignMonthlyIncome: $scope.contractData.foreignMonthlyIncome,
                nepaliMonthlyIncome: $scope.contractData.nepaliMonthlyIncome,
                nepaliMonthlyTotalIncome: nepaliIncome * contractPeroid,
                contractPeroid: $scope.contractData.contractPeriod,
                foreignPossibleIncome: $scope.contractData.foreignPossibleIncome,
                nepaliPossibleIncome: $scope.contractData.nepaliPossibleIncome,
                foreignHousingExpense: $scope.contractData.foreignHousingExpense,
                nepaliHousingExpense: $scope.contractData.nepaliHousingExpense,
                foreignFoodExpense: $scope.contractData.foreignFoodExpense,
                nepaliFoodExpense: $scope.contractData.nepaliFoodExpense,
                foreignPhoneExpense: $scope.contractData.foreignPhoneExpense,
                nepaliPhoneExpense: $scope.contractData.nepaliPhoneExpense,
                foreignTransportExpense: $scope.contractData.foreignTransportExpense,
                nepaliTransportExpense: $scope.contractData.nepaliTransportExpense,
                foreignOtherExpense: $scope.contractData.foreignOtherExpense,
                nepaliOtherExpense: $scope.contractData.nepaliOtherExpense,
                foreignTotalExpense: $scope.contractData.foreignTotalExpense,
                nepaliTotalExpense: $scope.contractData.nepaliTotalExpense,
                foreignTotalExpenseContract: $scope.contractData.foreignTotalExpenseContract,
                nepaliTotalExpenseContract: $scope.contractData.nepaliTotalExpenseContract,
                foreignTotalIncomeContract: $scope.contractData.foreignTotalIncomeContract,
                nepaliTotalIncomeContract: $scope.contractData.nepaliTotalIncomeContract,
                foreignTotalIncomes: $scope.contractData.foreignTotalIncomes,
                nepaliTotalIncomes: $scope.contractData.nepaliTotalIncomes

            }

            localStorageService.set('contractData', contractDatas);
            $state.transitionTo('app.dashboard', null, { 'reload': true });
        } else {
            $scope.submitted = true;
        }

    }


    if (localStorageService.get('contractData') !== null) {


        $scope.contractData.employeeName = localStorageService.get('contractData').employeeName;
        $scope.contractData.probation = parseInt(localStorageService.get('contractData').probation);
        $scope.contractData.foreignMonthlyIncome = localStorageService.get('contractData').foreignMonthlyIncome;
        $scope.contractData.contractPeriod = localStorageService.get('contractData').contractPeroid;
        $scope.contractData.foreignPossibleIncome = localStorageService.get('contractData').foreignPossibleIncome;
        $scope.contractData.foreignHousingExpense = localStorageService.get('contractData').foreignHousingExpense;
        $scope.contractData.foreignFoodExpense = localStorageService.get('contractData').foreignFoodExpense;
        $scope.contractData.foreignPhoneExpense = localStorageService.get('contractData').foreignPhoneExpense;
        $scope.contractData.foreignTransportExpense = localStorageService.get('contractData').foreignTransportExpense;
        $scope.contractData.foreignOtherExpense = localStorageService.get('contractData').foreignOtherExpense;
        $scope.contractData.foreignTotalExpense = localStorageService.get('contractData').foreignTotalExpense;
        $scope.contractData.foreignTotalExpenseContract = localStorageService.get('contractData').foreignTotalExpenseContract;
        $scope.contractData.foreignTotalIncomeContract = localStorageService.get('contractData').foreignTotalIncomeContract;
        $scope.contractData.foreignTotalIncomes = localStorageService.get('contractData').foreignTotalIncomes;




        $scope.contractData.nepaliMonthlyIncome = localStorageService.get('contractData').nepaliMonthlyIncome;
        $scope.contractData.nepaliPossibleIncome = localStorageService.get('contractData').nepaliPossibleIncome;
        $scope.contractData.nepaliHousingExpense = localStorageService.get('contractData').nepaliHousingExpense;
        $scope.contractData.nepaliFoodExpense = localStorageService.get('contractData').nepaliFoodExpense;
        $scope.contractData.nepaliTransportExpense = localStorageService.get('contractData').nepaliTransportExpense;
        $scope.contractData.nepaliPhoneExpense = localStorageService.get('contractData').nepaliPhoneExpense;
        $scope.contractData.foreignTransportExpense = localStorageService.get('contractData').foreignTransportExpense;
        $scope.contractData.nepaliOtherExpense = localStorageService.get('contractData').nepaliOtherExpense;
        $scope.contractData.nepaliTotalExpense = localStorageService.get('contractData').nepaliTotalExpense;
        $scope.contractData.nepaliTotalExpenseContract = localStorageService.get('contractData').nepaliTotalExpenseContract;
        $scope.contractData.nepaliTotalIncomeContract = localStorageService.get('contractData').nepaliTotalIncomeContract;
        $scope.contractData.nepaliTotalIncomes = localStorageService.get('contractData').nepaliTotalIncomes;

    }


    if ($scope.contractData.employeeName) {
        $scope.disabled = false;

    }















}
