angular.module('starter').controller('setupWizard',['$scope', '$rootScope', '$location', '$ionicPopup' ,setupWizard]);
function setupWizard($scope, $rootScope, $location, $ionicPopup) {
    $scope.selectedLanguage = $rootScope.language;
    
    //Registraion Code Validation
    $scope.submitCode = function(isValid) {
        // check to make sure the form is completely valid
        if (isValid) {
          $location.path('/registration-form');
        }
    };
}