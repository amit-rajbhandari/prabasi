angular.module('starter').controller('phoneRegistration',['$scope','$ionicModal', '$rootScope', '$location', '$ionicPopup' ,phoneRegistration]);
function phoneRegistration($scope, $ionicModal, $rootScope, $location, $ionicPopup) {
    $scope.selectedLanguage = $rootScope.language;
//Country Code
    $ionicModal.fromTemplateUrl('app/setupWizard/country-code.html', {
    scope: $scope
    })
    
    .then(function(modal) {
        $scope.countryCode = modal;
    });

    $scope.closeModal = function(dial_code,name) {
        $scope.countryCode.hide();
        $scope.countryData = {
            selectedCountry : name,
            countryCode : dial_code
        };
    };
    
    if($scope.selectedLanguage === 'Nepali') {
        $scope.countryData = {
            selectedCountry: 'तपाईंको देश'
        };
    }
    
    $scope.selectedCountry = {};
    
    $scope.countries = [{   
        name: "Nepal",
        dial_code: "+977",
        code: "NP"
    }, {
        name: "India",
        dial_code: "+91",
        code: "IN"
    }, {
        name: "Afghanistan",
        dial_code: "+93",
        code: "AF"
    }, {
        name: "Bangladesh",
        dial_code: "+880",
        code: "BD"
    }, {
        name: "Bhutan",
        dial_code: "+975",
        code: "BT"
    }, {
        name: "Maldives",
        dial_code: "+960",
        code: "MV"
    }, {
        name: "Pakistan",
        dial_code: "+92",
        code: "PK"
    }, {
        name: "Sri Lanka",
        dial_code: "+94",
        code: "LK"
    }, {
        name: "United Arab Emirates",
        dial_code: "+971",
        code: "AE"
    }, {
        name: "Saudi Arabia",
        dial_code: "+966",
        code: "SA"
    }, {
        name: "Qatar",
        dial_code: "+974",
        code: "QA"
    }, {

        name: "Malaysia",
        dial_code: "+60",
        code: "MY"
    }, {
        name: "Kuwait",
        dial_code: "+965",
        code: "KW"
    }, {
        name: "Oman",
        dial_code: "+968",
        code: "OM"
    }];

    $scope.countryData = {
        selectedCountry: $scope.countries[0].name,
        countryCode : $scope.countries[0].dial_code
    };

    $scope.phoneNumberRegistration = function(isValid) {
        if (isValid) {
          $location.path('/confirmation-code');
        } else {
            if($scope.selectedLanguage === 'Nepali'){
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'कृपया आवश्यक क्षेत्र भर्नुहोस्।'
                });
            } else { 
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'Please Fill The Required field.'
                });
            }
        }
    };
}