angular.module('starter').controller('userRegistration',['$scope', '$rootScope', '$location', '$ionicPopup' ,userRegistration]);
function userRegistration($scope, $rootScope, $location, $ionicPopup) {
    $scope.selectedLanguage = $rootScope.language;
    
    $scope.submitUserRegistration = function(isValid) {
        if (isValid) {
          $location.path('/country-selection');
        } else {
            if($scope.selectedLanguage === 'Nepali'){
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'कृपया आवश्यक क्षेत्र भर्नुहोस्।'
                });
            } else { 
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'Please Fill The Required field.'
                });
            }
        }
    };
    
    //Gender Selection   
    if($scope.selectedLanguage === 'Nepali'){
        $scope.userGenders = [
            {gender: 'पुरुष'}, 
            {gender: 'महिला'}, 
            {gender: 'तेस्रोलिङ्गी'}
        ]; 
    } else {
        $scope.userGenders = [
            {gender: 'Male'}, 
            {gender: 'Female'}, 
            {gender: 'Other'}
        ];
    }
    
    //Nationality
    if($scope.selectedLanguage === 'Nepali'){
        $scope.Nationalities = [
            {nation: "नेपाली"},
            {nation: "भारतीय"},
            {nation: "पाकिस्तानी"},
            {nation: "मलेशियन"},
            {nation: "बङ्लादेशी"},
            {nation: "भूटानी"},
            {nation: "माल्दिभियन"},
            {nation: "श्रीलङ्का"},
            {nation: "अमीरात"},
            {nation: "साउदी"},
            {nation: "कतारी"},
            {nation: "कुवैती"},
            {nation: "ओमानी"}
        ];
    } else {
        $scope.Nationalities = [
            {nation: "Nepali"},
            {nation: "Indian"},
            {nation: "Pakistani"},
            {nation: "Malaysian"},
            {nation: "Bangladeshi"},
            {nation: "Bhutanese"},
            {nation: "Maldivian"},
            {nation: "Sri Lankan"},
            {nation: "Emirati"},
            {nation: "Saudi"},
            {nation: "Qatari"},
            {nation: "Kuwaiti"},
            {nation: "Omani"}
        ];
    }
}