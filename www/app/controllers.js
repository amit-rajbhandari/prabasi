angular.module('starter').controller('defaultController', ['$scope', '$rootScope', '$ionicModal', '$timeout', '$cordovaInAppBrowser', 'localStorageService', '$state', defaultController]);

function defaultController($scope, $rootScope, $ionicModal, $cordovaInAppBrowser, $timeout, localStorageService, $state) {

    $scope.selectedCountry = "Nepal" //$rootScope.selectedValue.name;
    $scope.selectedLanguage = "English" //$rootScope.language;
    $scope.count = 0;
    $scope.visaData = {};
    $scope.ticketData = {};
    $scope.welfareData = {};
    $scope.labourData = {};
    $scope.readonly = false;
    $scope.readonly1 = false;
    $scope.readonly2 = false;
    $scope.readonly3 = false;


    /*currency*/
    $scope.currency = {
        "AED": 0.27,
        "SAR": 0.27,
        "QAR": 0.27,
        "MYR": 0.24,
        "KWD": 3.30,
        "OMR": 2.60
    };

    localStorageService.set('CurrencyDoller', $scope.currency);




    /*visa request submit*/
    $scope.VisaSubmit = function(isValid) {

        if (isValid) {
            var expense = $scope.visaData.expense;
            localStorageService.set('VisaRequest', expense);
            $state.transitionTo('app.dashboard', null, { 'reload': true });
        } else {
            $scope.submitted = true;
        }


    }

    if (localStorageService.get('VisaRequest') !== null) {
        $scope.visaData.expense = localStorageService.get('VisaRequest');

    }

    // if (localStorageService.get('BulkAmountData') != null) {

    //       if (localStorageService.get('BulkAmountData').VisaRequest != undefined && localStorageService.get('BulkAmountData').VisaRequest != false) {
    //           $scope.readonly3 = true;

    //           $scope.visaData.expense = localStorageService.get('VisaRequest');
    //       } else {

    //           $scope.visaData.expense = localStorageService.get('VisaRequest');
    //       }
    //   } else {

    //       $scope.visaData.expense = localStorageService.get('VisaRequest');
    //   }


    /*air ticket */

    $scope.AirTicketSubmit = function(isValid) {

        if (isValid) {
            var expense = $scope.ticketData.expense;
            localStorageService.set('AirTicket', expense);
            $state.transitionTo('app.dashboard', null, { 'reload': true });
        } else {
            $scope.submitted = true;
        }


    }

    if (localStorageService.get('AirTicket') !== null) {

        $scope.ticketData.expense = localStorageService.get('AirTicket');



    }

    // if (localStorageService.get('BulkAmountData') != null) {
    //         if (localStorageService.get('BulkAmountData').Airticket != undefined && localStorageService.get('BulkAmountData').Airticket != false) {
    //             $scope.readonly1 = true;


    //             $scope.ticketData.expense = localStorageService.get('AirTicket');
    //         } else {


    //             $scope.ticketData.expense = localStorageService.get('AirTicket');
    //         }
    //     } else {

    //         $scope.ticketData.expense = localStorageService.get('AirTicket');
    //     }


    /*for welfare fund*/


    $scope.WelfareSubmit = function(isValid) {

        if (isValid) {
            var fundCost = $scope.welfareData.fundCost;
            var extraCost = $scope.welfareData.additionalCost;
            var TotalCost = fundCost + extraCost;
            var welfareCost = {
                fundCost: $scope.welfareData.fundCost,
                additionalCost: $scope.welfareData.additionalCost,
                TotalCost: TotalCost
            }

            localStorageService.set('welfareCost', welfareCost);
            $state.transitionTo('app.dashboard', null, { 'reload': true });
        } else {
            $scope.submitted = true;
        }


    }


    if (localStorageService.get('welfareCost') !== null) {
        $scope.welfareData.fundCost = localStorageService.get('welfareCost').fundCost;
        $scope.welfareData.additionalCost = localStorageService.get('welfareCost').additionalCost;





    }

    if (localStorageService.get('BulkAmountData') != null) {

        if (localStorageService.get('BulkAmountData').WelfareCost != undefined && localStorageService.get('BulkAmountData').WelfareCost != false) {
            $scope.readonly2 = true;
            $scope.welfareData.fundCost = 0;
            if (localStorageService.get('welfareCost') !== null) {
                $scope.welfareData.additionalCost = localStorageService.get('welfareCost').additionalCost;
            }



        }
    } else {
        if (localStorageService.get('welfareCost') !== null) {
            $scope.welfareData.fundCost = localStorageService.get('welfareCost').fundCost;
            $scope.welfareData.additionalCost = localStorageService.get('welfareCost').additionalCost;


        }

    }






    /* labour permit submit*/
    $scope.LabourPermitSubmit = function(isValid) {
        if (isValid) {

            var expense = $scope.labourData.additionalExpense;
            localStorageService.set('labourData', expense);
            $state.transitionTo('app.dashboard', null, { 'reload': true });
        } else {
            $scope.submitted = true;
        }


    }

    if (localStorageService.get('labourData') !== null) {
        $scope.labourData.additionalExpense = localStorageService.get('labourData');




    }


    // if (localStorageService.get('BulkAmountData') != null) {

    //     if (localStorageService.get('BulkAmountData').LaborPermit != undefined && localStorageService.get('BulkAmountData').LaborPermit != false) {
    //         $scope.readonly = true;

    //         $scope.labourData.additionalExpense = localStorageService.get('labourData');
    //     } else {

    //         $scope.labourData.additionalExpense = localStorageService.get('labourData');
    //     }
    // } else {

    //     $scope.labourData.additionalExpense = localStorageService.get('labourData');
    // }



    /* bulk amount for manpower data*/
    if (localStorageService.get('BulkAmountData') !== null) {
        if (localStorageService.get('manpowerData') == null) {
            var manpowerData = {
                selectedManpowerCompany: '',
                manpowerCost: '',
                additionalCost: '',
                TotalCost: localStorageService.get('BulkAmountData').BulkAmount


            }
            localStorageService.set('manpowerData', manpowerData);
        }

        if (localStorageService.get('PassportCost') !== null) {

            if (localStorageService.get('BulkAmountData').Passport) {

                var passportCostData = {
                    passportCost: 0,
                    extraCost: localStorageService.get('PassportCost').extraCost,
                    TotalCost: localStorageService.get('PassportCost').extraCost


                }

                localStorageService.set('PassportCost', passportCostData);

            }


        }

        if (localStorageService.get('welfareCost') !== null) {

            if (localStorageService.get('BulkAmountData').WelfareCost) {

                var welfareCost = {
                    fundCost: 0,
                    additionalCost: localStorageService.get('welfareCost').additionalCost,
                    TotalCost: localStorageService.get('welfareCost').additionalCost
                }



                localStorageService.set('welfareCost', welfareCost);

            }


        }

        if (localStorageService.get('HealthCheckup') !== null) {

            if (localStorageService.get('BulkAmountData').HealthCheckup) {

                var healthcenterData = {
                    selectedHealthCenter: localStorageService.get('HealthCheckup').selectedHealthCenter,
                    healthCheckupFee: 0,
                    additionalCost: localStorageService.get('HealthCheckup').additionalCost,
                    TotalCost: localStorageService.get('HealthCheckup').additionalCost

                }
                localStorageService.set('HealthCheckup', healthcenterData);

            }


        }

        if (localStorageService.get('skillDevelopment') !== null) {

            if (localStorageService.get('BulkAmountData').SkillDevelopment) {

                var skillDevelopmentData = {
                    selectedTrainingCategory: localStorageService.get('skillDevelopment').selectedTrainingCategory,
                    categoryLang: gettrainingCenter(localStorageService.get('skillDevelopment').categoryLang),
                    selectedTrainingCenter: localStorageService.get('skillDevelopment').selectedTrainingCenter,
                    trainingFee: 0,
                    additionalCost: localStorageService.get('skillDevelopment').additionalCost,
                    TotalCost: localStorageService.get('skillDevelopment').additionalCost

                }
                localStorageService.set('skillDevelopment', skillDevelopmentData);

            }


        }

        if (localStorageService.get('InsuranceCost') !== null) {

            if (localStorageService.get('BulkAmountData').Insurance) {

                var insuranceCostData = {
                    premiumAmount: 0,
                    extraCost: localStorageService.get('InsuranceCost').extraCost,
                    TotalCost: localStorageService.get('InsuranceCost').extraCost

                }
                localStorageService.set('InsuranceCost', insuranceCostData);

            }


        }


        if (localStorageService.get('PreOrientationData') !== null) {

            if (localStorageService.get('BulkAmountData').PreOrientationTrainning) {

                var preOrientationData = {
                    selectedTrainningCenter: localStorageService.get('PreOrientationData').selectedTrainningCenter,
                    trainningCost: 0,
                    additionalCost: localStorageService.get('PreOrientationData').additionalCost,
                    TotalCost: localStorageService.get('PreOrientationData').additionalCost


                }
                localStorageService.set('PreOrientationData', preOrientationData);

            }


        }








    }





    /* for passport model */
    if (localStorageService.get('PassportCost') !== null) {
        $scope.PassportTotalCost = localStorageService.get('PassportCost').TotalCost;
        if ($scope.PassportTotalCost > 0) {
            $scope.count++;
        }
    }
    /* for health checkup*/
    if (localStorageService.get('HealthCheckup') !== null) {
        $scope.CheckupTotalCost = localStorageService.get('HealthCheckup').TotalCost;
        if ($scope.CheckupTotalCost > 0) {
            $scope.count++;
        }

    }

    /*for skill development*/
    if (localStorageService.get('skillDevelopment') !== null) {
        $scope.SkillDevelopmentTotalCost = localStorageService.get('skillDevelopment').TotalCost;

        if ($scope.SkillDevelopmentTotalCost > 0) {
            $scope.count++;
        }

    }

    /* for visa request*/


    if (localStorageService.get('VisaRequest') !== null) {
        $scope.VisaRequestCost = localStorageService.get('VisaRequest');
        if ($scope.VisaRequestCost > 0) {
            $scope.count++;
        }

    }

    /*for air ticket*/

    if (localStorageService.get('AirTicket') !== null) {
        $scope.AirTicketCost = localStorageService.get('AirTicket');
        if ($scope.AirTicketCost > 0) {
            $scope.count++;
        }

    }


    /* for insurance  */

    if (localStorageService.get('InsuranceCost') !== null) {
        $scope.InsuranceCostCost = localStorageService.get('InsuranceCost').TotalCost;
        if ($scope.InsuranceCostCost > 0) {
            $scope.count++;
        }

    }

    /* for welfare  */

    if (localStorageService.get('welfareCost') !== null) {
        $scope.WelfareCost = localStorageService.get('welfareCost').TotalCost;
        if ($scope.WelfareCost > 0) {
            $scope.count++;
        }

    }


    /*for contract */

    if (localStorageService.get('contractData') !== null) {
        $scope.MonthlyIncome = localStorageService.get('contractData').nepaliTotalIncomes;
        if ($scope.MonthlyIncome > 0) {
            $scope.count++;
        }

    }

    /*for contract */

    if (localStorageService.get('labourData') !== null) {
        $scope.LabourCost = localStorageService.get('labourData');
        if ($scope.LabourCost > 0) {
            $scope.count++;
        }

    }


    /*for pre-orientation trainning */

    if (localStorageService.get('PreOrientationData') !== null) {
        $scope.TrainningCost = localStorageService.get('PreOrientationData').TotalCost;
        if ($scope.TrainningCost > 0) {
            $scope.count++;
        }

    }



    /*for manpower cost */

    if (localStorageService.get('manpowerData') !== null) {
        $scope.ManpowerCost = localStorageService.get('manpowerData').TotalCost;
        if ($scope.ManpowerCost > 0) {
            $scope.count++;
        }



    }

    if (localStorageService.get('loanAmountDatas') !== null) {

        $scope.LoanAmount = localStorageService.get('loanAmountDatas').totalLoanAmounts;
        if ($scope.LoanAmount != undefined || $scope.LoanAmount != null) {
            $scope.count++;
        }

    }





    /*finanincial analysis*/

    if (localStorageService.get('contractData') != null) {
        var nepaliMonthlyIncome = localStorageService.get('contractData').nepaliMonthlyIncome;
        $scope.contractIncome = nepaliMonthlyIncome * 24;

        if (localStorageService.get('contractData').nepaliTotalExpense != undefined || localStorageService.get('contractData').nepaliTotalExpense != null) {
            var nepaliExpense = localStorageService.get('contractData').nepaliTotalExpense;
            $scope.livingExpense = nepaliExpense * 24;
        } else {
            $scope.livingExpense = 0;
        }

    }


    if (localStorageService.get('loanAmountDatas') != null) {
        if (localStorageService.get('loanAmountDatas').TotalLoanWithIntrest != undefined || localStorageService.get('loanAmountDatas').TotalLoanWithIntrest != null) {
            $scope.loanRepay = localStorageService.get('loanAmountDatas').TotalLoanWithIntrest;

        } else {
            $scope.loanRepay = 0;

        }



    }

    if ($scope.contractIncome == undefined) {
        $scope.TotalNetIncome = '';
        $scope.NetIncome = '';

    } else {
        $scope.TotalNetIncome = $scope.contractIncome - ($scope.livingExpense + $scope.loanRepay);

        $scope.NetIncome = ($scope.TotalNetIncome) / 24;


    }


    /*total processing expense in result page*/
    var passCost = 0;
    var healthConst = 0;
    var skillDevelopment = 0;
    var manPowerCosts = 0;
    var visaCosts = 0;
    var AirTicketCost = 0;
    var InsuranceCosts = 0;
    var welfare = 0;
    var leborCost = 0;
    var OrientationCost = 0;

    if (localStorageService.get('PassportCost') != null) {
        passCost = localStorageService.get('PassportCost').TotalCost;

    }

    if (localStorageService.get('HealthCheckup') != null) {
        healthConst = localStorageService.get('HealthCheckup').TotalCost;

    }
    if (localStorageService.get('skillDevelopment') != null) {
        skillDevelopment = localStorageService.get('skillDevelopment').TotalCost;

    }

    if (localStorageService.get('manpowerData') != null) {
        manPowerCosts = localStorageService.get('manpowerData').TotalCost;

    }
    if (localStorageService.get('VisaRequest') != null) {
        visaCosts = localStorageService.get('VisaRequest');

    }

    if (localStorageService.get('AirTicket') != null) {
        AirTicketCost = localStorageService.get('AirTicket');

    }
    if (localStorageService.get('InsuranceCost') != null) {
        InsuranceCosts = localStorageService.get('InsuranceCost').TotalCost;

    }

    if (localStorageService.get('welfareCost') != null) {
        welfare = localStorageService.get('welfareCost').TotalCost;

    }

    if (localStorageService.get('labourData') != null) {
        leborCost = localStorageService.get('labourData');

    }

    if (localStorageService.get('PreOrientationData') != null) {
        OrientationCost = localStorageService.get('PreOrientationData').TotalCost;

    }

    $scope.TotalProcessingExpense=passCost+healthConst+skillDevelopment+manPowerCosts+visaCosts+AirTicketCost+InsuranceCosts+welfare+leborCost+OrientationCost;
    


















}
