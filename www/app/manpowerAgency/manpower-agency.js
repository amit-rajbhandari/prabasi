angular.module('starter').controller('manpowerAgency', ['$scope', '$ionicModal', '$rootScope', '$ionicPopup', 'localStorageService', '$state', manpowerAgency]);

function manpowerAgency($scope, $ionicModal, $rootScope, $ionicPopup, localStorageService, $state) {
    $scope.selectedLanguage = "English" //$rootScope.language;
    $scope.lang = $scope.selectedLanguage;
    $scope.manpowers = [];
    $scope.status = false;
    var limit = 50;
    $scope.from = 0;
    $scope.to = $scope.from + limit;
    //Manpower Center Modal
    $ionicModal.fromTemplateUrl('app/manpowerAgency/manpower-agency-lists.html', {
        scope: $scope
    })

    .then(function(modal) {
        $scope.manpowerAgency = modal;
    });

    $scope.closeModal = function() {
        $scope.manpowerAgency.hide();
    };

    $scope.manpowerData = {
        selectedManpowerAgency: null,
        manPowerFee: null,
        additionalCost: null
    };

    function init() {
        var result = getCompanies($scope.from, $scope.to, $scope.lang);
        for (var i = 0; i < result.length; i++) {
            $scope.manpowers.push(result[i]);
        }
    }

    init();
    $scope.loadmoreCompany = function() {
        $scope.from = $scope.to;
        $scope.to = $scope.from + limit;
        var result = getCompanies($scope.from, $scope.to, $scope.lang);
        for (var i = 0; i < result.length; i++) {
            $scope.manpowers.push(result[i]);
        }
    };



    if (localStorageService.get('manpowerData') !== null && localStorageService.get('BulkAmountData') == null) {
        $scope.manpowerData.selectedManpowerAgency = localStorageService.get('manpowerData').selectedManpowerCompany;
        $scope.manpowerData.manpowerFee = localStorageService.get('manpowerData').manpowerCost;
        $scope.manpowerData.additionalFee = localStorageService.get('manpowerData').additionalCost;
    }


    if (localStorageService.get('BulkAmountData') != null) {

        $scope.manpowerData.manpowerFee = localStorageService.get('BulkAmountData').BulkAmount;
        // var manpower=localStorageService.get('manpowerData');
        // console.log(manpower);
                // var manPowercost = $scope.manpowerData.manpowerFee;
                // var additiCost = $scope.manpowerData.additionalFee;

        if (localStorageService.get('manpowerData') != null) {
            $scope.manpowerData.selectedManpowerAgency = localStorageService.get('manpowerData').selectedManpowerCompany;
            $scope.manpowerData.additionalFee = localStorageService.get('manpowerData').additionalCost;
            $scope.status = true;

        } else {
            $scope.manpowerData.selectedManpowerAgency = '';
            $scope.manpowerData.additionalFee = '';
            $scope.status = true;

        }


    }


    $scope.ManpowerSubmit = function(isValid) {



        if (isValid) {
            var manpowerCompany = $scope.manpowerData.selectedManpowerAgency;
            if (manpowerCompany == null || manpowerCompany == '') {
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'Please select manpower company.'
                });

            } else {
                var manpowerCost = $scope.manpowerData.manpowerFee;
                var additionalCost = $scope.manpowerData.additionalFee;
                if (additionalCost == undefined || additionalCost == null) {
                    additionalCost = 0;
                }
                var TotalCost = manpowerCost + additionalCost;
                var manpowerData = {
                    selectedManpowerCompany: manpowerCompany,
                    manpowerCost: manpowerCost,
                    additionalCost: additionalCost,
                    TotalCost: TotalCost,


                }

                localStorageService.set('manpowerData', manpowerData);

                $state.transitionTo('app.dashboard', null, { 'reload': true });


            }

        } else {
            $scope.submitted = true;
        }



    }








}
