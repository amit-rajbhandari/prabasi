angular.module('starter').controller('countrySelection', ['$scope', '$rootScope', '$ionicPopup', '$location', 'localStorageService', countrySelection]);

function countrySelection($scope, $rootScope, $ionicPopup, $location, localStorageService) {
    // $scope.selectedLanguage = $rootScope.language;
    $scope.selectedLanguage = "English";
    if ($scope.selectedLanguage === 'Nepali') {
        $scope.countryLists = [
            { name: 'सयुंक्त अरब इमिरेट्स' },
            { name: 'साउदी अरेबिया' },
            { name: 'कतार' },
            { name: 'मलेसिया' },
            { name: 'कुवेत' },
            { name: 'ओमान' }
        ];
    } else {
        $scope.countryLists = [
            { name: 'United Arab Emirates' },
            { name: 'Saudi Arabia' },
            { name: 'Qatar' },
            { name: 'Malyasia' },
            { name: 'kuwait' },
            { name: 'Oman' }
        ];
    }

    $scope.changeValue = function changeValue(countryList) {
        $rootScope.selectedValue = countryList;
        var countryName = $rootScope.selectedValue;

    };
    $scope.process = {};

    $scope.processNew = function(isValid) {
        if (($rootScope.selectedValue) && (isValid)) {
            var processData = {
                processName: $scope.process.name,
                country: $rootScope.selectedValue.name

            }

            localStorageService.set('CountrySelection', processData);

            $location.path('/select-features');
        } else if (!(isValid)) {
            if ($scope.selectedLanguage === 'Nepali') {
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'कृपया आवश्यक क्षेत्र भर्नुहोस्।'
                });
            } else {
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'Please Fill The Required field.'
                });
            }
        } else {
            if ($scope.selectedLanguage === 'Nepali') {
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'कृपया देश छानुहोस'
                });
            } else {
                $ionicPopup.alert({
                    title: '<i class="ion ion-alert"></i>',
                    template: 'Please select the country'
                });
            }
        }
    };
}
