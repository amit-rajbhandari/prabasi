angular.module('starter').controller('insuranceController', ['$scope', '$state', '$rootScope', 'localStorageService', '$window', function($scope, $state, $rootScope, localStorageService, $window) {
    $scope.insuranceData = {};
    $scope.readonly = false;
    if (localStorageService.get('InsuranceCost') !== null) {
        $scope.insuranceData.premiumAmount = localStorageService.get('InsuranceCost').premiumAmount;
        $scope.insuranceData.additionalCost = localStorageService.get('InsuranceCost').extraCost;


    }


    if (localStorageService.get('BulkAmountData') != null) {

        if (localStorageService.get('BulkAmountData').Insurance != undefined && localStorageService.get('BulkAmountData').Insurance != false) {
            $scope.readonly = true;
            $scope.insuranceData.premiumAmount = 0;
            if (localStorageService.get('InsuranceCost') !== null)
            {
                  $scope.insuranceData.additionalCost = localStorageService.get('InsuranceCost').extraCost;

            }
          


        
    } else {
        if(localStorageService.get('InsuranceCost') !== null)
        {
        $scope.insuranceData.premiumAmount = localStorageService.get('InsuranceCost').premiumAmount;
        $scope.insuranceData.additionalCost = localStorageService.get('InsuranceCost').extraCost;
        }
       

    }
}

    $scope.InsuranceSubmit = function(isValid) {
        if (isValid) {
            var premiumAmount = $scope.insuranceData.premiumAmount;
            var extraCost = $scope.insuranceData.additionalCost;
            var TotalCost = premiumAmount + extraCost;
            var insuranceCostData = {
                premiumAmount: premiumAmount,
                extraCost: extraCost,
                TotalCost: TotalCost

            }
            localStorageService.set('InsuranceCost', insuranceCostData);
            $state.transitionTo('app.dashboard', null, { 'reload': true });


        } else {
            $scope.submitted = true;
        }


    }












}])
